import os
from multiprocessing import Process

from programme.programme_robot import sensors_callback
from internal.tb3_node import launch_robot
from internal.display import Display
from internal._utils import _get_ip


def robot_process():
    launch_robot(sensors_callback)


def display_process():
    d = Display()
    d.mainloop()


if __name__ == "__main__":
    os.environ["ROS_MASTER_URI"] = "http://192.168.1.1:11311"
    IP = _get_ip()
    print(IP)
    os.environ["ROS_IP"] = IP

    p_robot = Process(target=robot_process)
    p_display = Process(target=display_process)

    p_robot.start()
    p_display.start()

    p_robot.join()
    p_display.join()
