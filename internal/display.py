import tkinter as tk
import rospy

from std_srvs.srv import SetBool


class Display(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Pixels robot")
        self.geometry("600x350")

        rospy.init_node("turtlebot_control")
        self._client = rospy.ServiceProxy("enable_control", SetBool)

        start_button = tk.Button(
            self, text="Start", command=lambda: self._client(True)
        ).pack()

        stop_button = tk.Button(
            self, text="Stop", command=lambda: self._client(False)
        ).pack()

        rospy.loginfo("Enable service client is ready")
