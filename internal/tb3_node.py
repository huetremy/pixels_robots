import rospy
import cv2
import numpy as np
import math

from std_msgs.msg import Int32
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse
from sensor_msgs.msg import LaserScan, CompressedImage
from geometry_msgs.msg import Twist

_node = None
SCALE_LINEAR = 0.22
SCALE_ANGULAR = 2.84


def bouge(vitesse_lineaire, vitesse_angulaire):
    global _node

    if _node is None:
        rospy.logwarn("Robot node is not running")
        return

    if not _node._control_enabled:
        rospy.logwarn("Robot control disabled")
        return

    twist = Twist()
    twist.linear.x = vitesse_lineaire * SCALE_LINEAR
    twist.angular.z = vitesse_angulaire * SCALE_ANGULAR

    rospy.loginfo("Mooving robot")
    _node._twist_publisher.publish(twist)


def stop():
    global _node

    if _node is None:
        rospy.logwarn("Robot node is not running")
        return

    twist = Twist()
    twist.linear.x = 0
    twist.angular.z = 0

    _node._twist_publisher.publish(twist)


def log(message):
    rospy.loginfo(message)


def shutdown():
    log("Shutting down node")
    stop()


class TB3Node:
    def __init__(self, sensors_callback) -> None:
        rospy.init_node("tb3_manager")
        rospy.loginfo("Initializing tb3 manager")

        self._last_image = None
        self._last_ultrasound = None
        self._sensors_callback = sensors_callback

        rospy.loginfo("Subscribing to ultrasound sensor")
        self._ultrasound_subscriber = rospy.Subscriber(
            "/ultrasound", Int32, self._ultrasound_callback
        )

        rospy.loginfo("Subscribing to camera")
        self._camera_subscriber = rospy.Subscriber(
            "/turtlebotcam/image_raw/compressed", CompressedImage, self._camera_callback
        )

        rospy.loginfo("Subscribing to lidar sensor")
        self.lidar_subscriber = rospy.Subscriber(
            "/scan", LaserScan, self._lidar_callback
        )

        rospy.loginfo("Setup robot control switch")
        self._enable_service = rospy.Service(
            "enable_control", SetBool, self._enable_service_callback
        )
        self._control_enabled = False
        rospy.loginfo("Robot control is disabled")

        rospy.loginfo("Setup twist publisher")
        self._twist_publisher = rospy.Publisher("/cmd_vel", Twist, queue_size=10)

        rospy.loginfo("Setup finished")

    def _ultrasound_callback(self, msg: Int32) -> None:
        self._last_ultrasound = msg.data

    def _camera_callback(self, msg: CompressedImage) -> None:
        data = np.frombuffer(msg.data, np.uint8)
        img = cv2.imdecode(data, cv2.IMREAD_COLOR)
        cv2.imshow("Image", img)
        cv2.waitKey(2)
        self._last_image = img

    def _lidar_callback(self, msg: LaserScan) -> None:
        if self._last_image is None or self._last_ultrasound is None:
            return

        points = []
        for i, theta in enumerate(
            np.arange(msg.angle_min, msg.angle_max, msg.angle_increment)
        ):
            angle = math.degrees(theta)
            points.append((angle, msg.ranges[i]))

        self._sensors_callback(self._last_ultrasound, self._last_image, points)

    def _enable_service_callback(self, msg: SetBoolRequest) -> SetBoolResponse:
        response = SetBoolResponse()

        if msg.data:
            self._control_enabled = True
            rospy.loginfo("Robot control is enabled")
        else:
            self._control_enabled = False
            stop()
            rospy.loginfo("Robot control is disabled")

        response.success = True
        return response


def launch_robot(sensors_callback):
    try:
        global _node
        _node = TB3Node(sensors_callback)
        rospy.on_shutdown(shutdown)
        rospy.spin()

    except rospy.ROSInterruptException:
        stop()


if __name__ == "__main__":
    launch_robot()
